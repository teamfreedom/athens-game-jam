﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Assets.Scripts
{

    public class HoganController : Controller, IPlayerControlled, IKillable
    {
        public Camera MainCamera;
        public bool IsAttacking;
        public float Cooldown;

        public float JumpVel;

        public GameObject SmashAtkAgentPrefab;
        public GameObject SlamAtkAgentPrefab;

        public Transform SmashAtkSocket;
        public Transform SlamAtkSocket;

        public int MaxHealth;
        private int _currentHealth;

        public Slider HealthSlider;

        void Awake()
        {
            MainCamera = Camera.main;
            MyRigidbody = GetComponent<Rigidbody>();
            MyAnimator = GetComponent<Animator>();

            MyRigidbody.freezeRotation = true;

            _currentHealth = MaxHealth;
        }

        void Update()
        {
            HandleInputs();

            if (Cooldown > 0) 
                Cooldown -= Time.deltaTime;
            else
            {
                IsAttacking = false;
            }
        }

        void FixedUpdate()
        {
            Move();
            LookAtTarget();
        }


        public void HandleInputs()
        {
            XMove = Input.GetAxis("Xmove");
            ZMove = Input.GetAxis("Zmove");

            Ray mouseRay = MainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit mouseHit;

            if (Physics.Raycast(mouseRay, out mouseHit))
                LookTarget = new Vector3(mouseHit.point.x, transform.position.y, mouseHit.point.z);

            if (Input.GetAxis("SmashAtk") > 0f)
            {
                Attack(AtkType.Smash);
            }

            if (Input.GetAxis("BodySlam") > 0f)
            {
                Attack(AtkType.Slam);
            }
        }



        public enum AtkType
        {
            Smash = 0,
            Slam = 1
        }


        public void Attack(AtkType atk)
        {
            if (!IsAttacking)
            {
                switch (atk)
                {
                    case AtkType.Smash:
                        IsAttacking = true;
                        SmashAtk();
                        break;
                    case AtkType.Slam:
                        IsAttacking = true;
                        SlamAtk();
                        break;
                }
            }
        }



        public void SmashAtk()
        {
            MyAnimator.SetTrigger("boot");

            GameObject atkAgent =
                Instantiate(
                    SmashAtkAgentPrefab,
                    SmashAtkSocket.position,
                    Quaternion.identity) 
                as GameObject;

            atkAgent.transform.SetParent(transform);
            Cooldown = atkAgent.GetComponent<AtkAgentController>().AttackDelayTime;
        }


        public void SlamAtk()
        {
            MyAnimator.SetTrigger("legDrop");
            MyRigidbody.velocity += new Vector3(0, JumpVel, 0);

            GameObject atkAgent =
                Instantiate(
                    SlamAtkAgentPrefab,
                    SlamAtkSocket.position,
                    Quaternion.identity)
                as GameObject;

            atkAgent.transform.SetParent(transform);
            Cooldown = atkAgent.GetComponent<AtkAgentController>().AttackDelayTime;
        }




        public void Die()
        {
            print("DED.");
        }



        public void TakeDamage(int damage)
        {
            _currentHealth -= damage;
            HealthSlider.value = _currentHealth;

            if (_currentHealth <= 0)
                Die();
        }
    }
}
