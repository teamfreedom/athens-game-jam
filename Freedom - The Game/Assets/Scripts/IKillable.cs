﻿namespace Assets.Scripts
{
    public interface IKillable
    {
        void Die();
        void TakeDamage(int damage);
    }
}
