﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class KoolaidController : Controller, IKillable
    {

        public NavMeshAgent MyNavMeshAgent;
        public Transform Target;

        public float AttackRange;
        public float AttackCooldown;
        private float _cooldown;
        public int AttackDamage;

        public int MaxHealth;
        private int _currentHealth;

        public Transform GunSocket;

        public GameObject DeathEffectPrefab;
        public GameObject AttackEffectPrefab;



        void Awake()
        {
            MyNavMeshAgent = GetComponent<NavMeshAgent>();
            MyNavMeshAgent.stoppingDistance = AttackRange - 1;
            RotationSpeed = 4;

            MyAnimator = GetComponent<Animator>();

            _currentHealth = MaxHealth;
        }


        void Update()
        {
            if (Target == null)
            {
                Target = GameObject.FindGameObjectWithTag("Hulk").transform;
            }
            else
            {
                LookTarget = Target.position;
            }

            MyNavMeshAgent.SetDestination(Target.position);

            AttackTarget();
        }


        public void AttackTarget()
        {
            if (Vector3.Distance(transform.position, Target.position) <= AttackRange)
            {
                LookAtTarget();
                ShootAtk();
            }
        }


        public void ShootAtk()
        {
            if (_cooldown <= 0)
            {
                Destroy(Instantiate(AttackEffectPrefab, GunSocket.position, GunSocket.rotation), AttackCooldown);

                IKillable killableTarget = (IKillable) Target.GetComponent<MonoBehaviour>();
                if (killableTarget != null)
                {
                    killableTarget.TakeDamage(AttackDamage);
                }

                _cooldown = AttackCooldown;
            }
            else
            {
                _cooldown -= Time.deltaTime;
            }
        }

        public void Die()
        {
            Destroy(Instantiate(DeathEffectPrefab, transform.position + new Vector3(0, .5f, 0), Quaternion.identity), 1.5f);
            Destroy(gameObject);
        }

        public void TakeDamage(int damage)
        {
            _currentHealth -= damage;

            if (_currentHealth <= 0)
                Die();
        }
    }
}