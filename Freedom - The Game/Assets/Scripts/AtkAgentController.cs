﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class AtkAgentController : MonoBehaviour
    {
        public int AttackDamage;
        public float AttackDelayTime;

        public bool DamageCaused;
        public String IgnoreTag;

        private float _timer;

        public List<Collider> AllCollidersInRange;



        void Awake()
        {
            _timer = AttackDelayTime;
        }


        void Update()
        {
            _timer -= Time.deltaTime;

            if (!DamageCaused && _timer <= 0)
                CauseDamage();

            if (DamageCaused) Destroy(gameObject);
        }



        public void CauseDamage()
        {
            List<IKillable> allKillablesInRange = new List<IKillable>();

            foreach (Collider col in AllCollidersInRange)
            {
                if (col.tag != IgnoreTag)
                {
                    print(col.tag);
                    MonoBehaviour mb = col.GetComponent<MonoBehaviour>();

                    IKillable killable = mb as IKillable;

                    if (killable != null)
                    {
                        allKillablesInRange.Add(killable);
                    }
                }
            }


            foreach (IKillable killable in allKillablesInRange)
            {
                killable.TakeDamage(AttackDamage);
            }

            DamageCaused = true;
        }


        void OnTriggerEnter(Collider other)
        {
            AllCollidersInRange.Add(other);
        }

        void OnTriggerExit(Collider other)
        {
            AllCollidersInRange.Remove(other);
        }
    }
}
