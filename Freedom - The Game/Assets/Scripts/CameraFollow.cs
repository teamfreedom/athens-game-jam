﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class CameraFollow : MonoBehaviour
    {
        public Transform Target;
        public Vector3 Offset;
        public float FollowSpeed;

        void Awake()
        {
            if (Target == null)
                Target = GameObject.FindGameObjectWithTag("Hulk").transform;
        }

        void LateUpdate()
        {
            FollowTarget();
        }


        public void FollowTarget()
        {
            Vector3 desiredPosition = Target.position + Offset;

            transform.position = Vector3.Slerp(transform.position, desiredPosition, FollowSpeed * Time.deltaTime);
        }
    }
}