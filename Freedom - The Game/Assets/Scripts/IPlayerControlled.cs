﻿namespace Assets.Scripts
{
    public interface IPlayerControlled
    {
        void HandleInputs();
    }
}
