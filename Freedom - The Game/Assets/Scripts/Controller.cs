﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Animator))]
    public class Controller : MonoBehaviour
    {

        public float MoveSpeed;
        public float MoveSpeedMult;
        public float RotationSpeed;

        public float XMove;
        public float ZMove;
        public Vector3 LookTarget;

        public Rigidbody MyRigidbody;
        public Animator MyAnimator;





        public void LookAtTarget()
        {
            //Debug.DrawLine(transform.position, LookTarget, Color.red, 0.1f);

            Vector3 direction = (LookTarget - transform.position).normalized;

            Quaternion lookRotation = Quaternion.LookRotation(direction);

            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, RotationSpeed * Time.fixedDeltaTime);

            print("Looking");

            //transform.LookAt(LookTarget);

            //Debug.Log("Rigidbody's rotation: " + MyRigidbody.rotation.eulerAngles + ", Transform rotation: " + transform.rotation.eulerAngles);
        }




        public void Move()
        {
            MyRigidbody.velocity = new Vector3
                (
                XMove * MoveSpeed * MoveSpeedMult, 
                MyRigidbody.velocity.y, 
                ZMove * MoveSpeed * MoveSpeedMult
                );

            MyAnimator.SetFloat("running", new Vector2(XMove, ZMove).magnitude);
        }
    }
}
