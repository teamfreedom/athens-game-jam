﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class SmashableObject : MonoBehaviour, IKillable
    {

        public int MaxHealth;
        private int _currentHealth;

        public GameObject DeathEffect;


        void Awake()
        {
            _currentHealth = MaxHealth;
        }



        public void Die()
        {
            Destroy(Instantiate(DeathEffect, transform.position, Quaternion.identity), 1.5f);
            Destroy(gameObject);
        }

        public void TakeDamage(int damage)
        {
            _currentHealth -= damage;

            if (_currentHealth <= 0)
                Die();
        }
    }
}